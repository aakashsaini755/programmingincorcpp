#include <iostream>
// #include "solution.h"
using namespace std;
void merge(int input[],int start,int mid,int end){
     int l=mid-start+1;
    int m=end-mid;
    
    int arr1[l],arr2[m];
    
    for(int i=0;i<l;i++)
        arr1[i]=input[start+i];
    for(int j=0;j<m;j++)
        arr2[j]=input[mid+1+j];
    
    int i=0,j=0,k=start;
    while(i<l && j<m)
    {
        if(arr1[i]<=arr2[j])
        {
            input[k]=arr1[i];
            i++;
        }
        else
        {
            input[k]=arr2[j];
            j++;            
        }
        k++;
    }
    
    while(i<l)
    {
        input[k]=arr1[i];
            i++;
        k++;
    }
    
    while(j<m)
    {
        input[k]=arr2[j];
            j++;
        k++;
    }
}
void mergeHelper(int input[]),int size, int start){
    if(start >= size){
        return;
    }
    int mid = (start+size)/2;
    mergeHelper(input,mid,start);
    mergeHelper(input,size,mid + 1);
    merge(input,start,mid,end);

}
void mergeSort(int input[],int size){
    if(size == 0 || size == 1){
        return;
    }
    mergeHelper(input,size-1,0);
}

int main() {
  int length;
  cin >> length;
  int* input = new int[length];
  for(int i=0; i < length; i++)
    cin >> input[i];
  mergeSort(input, length);
  for(int i = 0; i < length; i++) {
    cout << input[i] << " ";
  }
  return 0;
}