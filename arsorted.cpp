#include<iostream>
using namespace std;
bool is_sorted(int a[], int size) {
	if (size == 0 || size ==1) {
		return true;
	}

	if (a[0] > a[1]) {
		return false;
	}
	bool isSmallerSorted = is_sorted(a + 1, size - 1);
	return isSmallerSorted;
}
int main(){
    int a[]={5, 6, 12, 8, 9, 10};
    int size=6;
    // cin>>size;
    cout<<is_sorted(a,size)<<endl;
    return 0;

}