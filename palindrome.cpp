#include<iostream>
#include<cstring>
using namespace std;
bool helper(char str[],int start,int end){
    if(start >= end){
        return true;
    }
    if(str[start] != str[end]){
        return false;
    }
    return helper(str,start+1,end-1);
}
bool checkPalindrome(char str[]){
    return helper(str,0,strlen(str)-1);
}

int main(){
      char input[50];
      cin >> input;
    
    if(checkPalindrome(input)) {
        cout << "true" << endl;
    }
    else {
        cout << "false" << endl;
    }
}