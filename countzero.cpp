#include<iostream>
using namespace std;

int helper(int n){
    if(n == 0){
        return 0;
    }
    if(n%10 == 0){
        return helper(n/10)+1;
    }else{
        return helper(n/10);
    }
}
int countZero(int n){
    if(n == 0){
        return 1;
    }
    return helper(n);
}
int main(){
    int n;
    cin>>n;
    cout<<countZero(n);
    return 0;
}