#include<iostream>
using namespace std;
int sum(int input[], int n){
    if(n == 1){
        return input[0];
    }
    int smallsum = sum(input+1,n-1);
    return smallsum+input[0];
}
int main(){
    int n;
    cin >> n;
  
    int *input = new int[n];
    
    for(int i = 0; i < n; i++) {
        cin >> input[i];
    }
    
    cout << sum(input, n) << endl;
    return 0;
}