#include<iostream>
// #include "Solution.h"
using namespace std;
int partition(int input[],int s,int e){
    int x = input[s];
    int count = 0;
    for(int i=s+1;i<=e;i++){
        if(input[i] <= x){
            count++;
        }
    }
    int temp = input[s];
        input[s] = input[s+count];
        input[s+count] = temp;

     int i=s,j=e;
    while(i<s+count && j>s+count){
        if(input[i]<= input[s+count])
         i++;
         else if(input[j]>input[s+count])
         j--;
         else{
             int temp =  input[i];
             input[i] = input[j];
             input[j] = temp;
             i++;
             j--;
         }
    }
    return s+count;
    
}
void quickHelper(int input[],int start,int end){
    if(start >=end){
        return;
    }
    int c = partition(input,start,end);
    quickHelper(input,start,c-1);
    quickHelper(input,c+1,end);

}
void quickSort(int input[],int size){
  if(size == 0 || size ==1){
      return;
  }
  quickHelper(input,0,size-1);
}

int main(){
    int n;
    cin >> n;
  
    int *input = new int[n];
    
    for(int i = 0; i < n; i++) {
        cin >> input[i];
    }
    
    quickSort(input, n);
    for(int i = 0; i < n; i++) {
        cout << input[i] << " ";
    }
    
    delete [] input;
  return 0;
}


