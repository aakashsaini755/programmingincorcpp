#include<iostream>
#include<math.h>
using namespace std;
double geometricSum(int n){
    if(n ==0){
        return 1;
    }
    double a = 1/pow(2,n);
    double ans = geometricSum(n-1);
    return ans+a;
}
int main(){
    int n;
    cin>>n;
    cout<< geometricSum(n)<<endl;
    return 0;
}