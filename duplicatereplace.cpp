#include <iostream>
using namespace std;
// #include "solution.h"
void removeConsecutiveDuplicates(char *s){
    if(s[0] == '\0'){
        return;
    }
    if(s[0] == s[1]){
        for(int i=1; i<s[i]!='\0'; i++){
            s[i] = s[i+1];
        }
         removeConsecutiveDuplicates(s);
    }
        removeConsecutiveDuplicates(s + 1);
}
int main() {
    char s[100000];
    cin >> s;
    removeConsecutiveDuplicates(s);
    cout << s << endl;
}